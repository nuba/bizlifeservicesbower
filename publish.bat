@ECHO off
set ver=%1
IF "%ver%" == "" GOTO faltaParametro

ECHO Vamos a crear un paquete con el siguiente n�mero: %ver%
set makeRelease=n
set /p makeRelease=Desea continuar? [y/n] (default - n)?:
IF %makeRelease% == n GOTO exit
set gitAdd=n
set /p gitAdd=Quiere agregar elementos nuevos al repositorio? [y/n] (default - n)?:
IF %gitAdd% == n GOTO gitCommit
git add .
:gitCommit
echo Creando paquete
git commit -am "Release version %ver%"
git tag -a %ver% -m "Release version %ver%"
git push origin master --tags
goto exit
:faltaParametro
echo ERROR: No se ha ingresado el parametro con el n�mero de versi�n.
:help
echo publish [version-number]
echo Ejemplo: publish 0.0.1
:exit
echo .