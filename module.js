'use strict';
angular.module('bizlifeservice', ['AdalAngular']);
'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.service
 * @description
 * # service
 * Provider in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .provider('$hostUrl', function () {
    // Private variables
    var hostUrl = 'http://localhost:55986';

    // Private constructor
    function HostUrl() {
      this.url = function () {
        return hostUrl;
      };
    }

    // Public API for configuration
    this.setUrl = function (url) {
      hostUrl = url;
    };

    // Method for instantiating
    this.$get = function () {
      return new HostUrl();
    };
  });

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.uniqueIdService
 * @description
 * # uniqueIdService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('uniqueIdService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var provider = $hostUrl;
    var uService = {};
    
    uService.getUrl = function(uid){
      return $http.get(provider.url()+'/api/uniqueids/url/'+uid);
    };

    uService.getUid = function(data){
      return $http.post(provider.url() + '/api/uniqueids', data);
    };
    
    uService.getBusinessDataInfo = function(data){
      return $http.get(provider.url() + '/api/businesstypes/' + data.uid);
    };
    
    return uService;

  }]);


'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.fileservice
 * @description
 * # fileservice
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('fileService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var provider = $hostUrl;
    var fService = {};

    fService.delete = function(file){
      return $http.delete(provider.url() + '/api/files' + file.id);
    };

    fService.update = function(file){
      return $http.put(provider.url() + '/api/files' + file.id, file);
    };

    fService.getByUniqueId = function(uniqueid, quantity){
      return $http.get(provider.url() + '/api/uniqueid' + uniqueid + '/files/' + quantity);
    };

    fService.getById = function(id){
      return $http.get(provider.url() + '/api/files' + id);
    };

    fService.getByUniqueIdAndContentType = function(file){
      return $http.post(provider.url() + '/api/files/uniqueId', file);
    };

    fService.save = function (payload) {
      return $http({
        url: provider.url() + '/api/files',
        method: 'POST',
        data: payload,
        headers: { 'Content-Type': undefined },
        transformRequest: angular.identity
      });
    };

    return fService;
  }]);

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.commentService
 * @description
 * # commentService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('commentService', ["$http", "$hostUrl", function ($http, $hostUrl) {
  var provider = $hostUrl;  
  var cService = {};
  
  cService.save = function(comment){
    return $http.post(provider.url() + '/api/comments', comment);
  };
  
  cService.getByUniqueId = function(uniqueId, quantity){
    if(quantity === undefined){
      
      return $http.get(provider.url() + '/api/uniqueid/' + uniqueId  + '/comments/' + 0);
    }
    else{
      return $http.get(provider.url() + '/api/uniqueid/' + uniqueId  + '/comments/' + quantity);
    }
  };

  cService.search = function(uniqueIdApp, quantity){
    var filter = {
        searchText: "",
        page: 1,
        quantity: quantity,
        uid: uniqueIdApp,
        user: "",
        date: null
      };
      return $http.post(provider.url() + '/api/comments/filter',filter);
  };

  cService.update = function(id, comment){
    return $http.put(provider.url() + '/api/comments/' + id, comment);
  };

  cService.getByUser = function(user, quantity){
    return $http.get(provider.url() + '/api/user/' + user + '/comments/' + quantity);
  };

  return cService;
}]);
    
'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.trackingservice
 * @description
 * # trackingservice
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('trackingService', ["$http", "uniqueIdService", "$q", "$hostUrl", function ($http, uniqueIdService, $q, $hostUrl) {
    var tService = {};
    var provider = $hostUrl;

    tService.save = function (tracking) {
      var promise = $q.defer();
      uniqueIdService.getBusinessDataInfo(tracking).then(function (vals){
        tracking.UIdApp = vals.data;
        $http.post(provider.url() + '/api/trackings', tracking).then(promise.resolve, promise.reject); 
      },function(error){
        return promise.reject(error);
      });
      return promise.promise;
    };

    tService.getAll = function (uid) {
      return $http.get(provider.url() + '/api/uniqueid/' + uid + '/trackings/' + 0);
    };

    tService.getByUniqueId = function (uid, quantity) {
      return $http.get(provider.url() + '/api/uniqueid/' + uid + '/trackings/' + quantity);
    };

    tService.getByUniqueIdApp = function (uniqueIdApp, quantity) {
      return $http.get(provider.url() + '/api/uniqueIdApp/' + uniqueIdApp + '/trackings/' + quantity);
    };

    tService.getByUser = function (user, quantity) {
      return $http.get(provider.url() + '/api/user/' + user + '/trackings/' + quantity);
    };

    return tService;
  }]);

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.logginservice
 * @description
 * # logginservice
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('loggingService', ["$http", "$hostUrl", "uniqueIdService", "$q", function ($http, $hostUrl, uniqueIdService, $q) {
    var provider = $hostUrl;
    var lService = {};

    lService.save = function (loggin) {
      var promise = $q.defer();
      uniqueIdService.getBusinessDataInfo(loggin).then(function (vals){
        loggin.UIdApp = vals.data;
        $http.post(provider.url() + '/api/loggings', loggin).then(promise.resolve, promise.reject);
      },function(error){
        return promise.reject(error);
      });
      return promise.promise;
    };

    lService.getByUniqueIdApp = function (uniqueIdApp, quantity) {
      return $http.get(provider.url() + '/api/uniqueIdApp/' + uniqueIdApp + '/loggings/' + quantity);
    };

    lService.getByUser = function (user, quantity) {
      return $http.get(provider.url() + '/api/user/' + user + '/loggings/' + quantity);
    };

    return lService;

  }]);

'use strict';

/**
 * @ngdoc filter
 * @name bizlifeservice.filter:getId
 * @function
 * @description
 * # getId
 * Filter in the bizlifeservice.
 */
angular.module('bizlifeservice')
.filter('getId', function () {
  function getId(uId) {
    var res = uId.split('-');
    return res[1];
  }

  return getId;
});

'use strict';

/**
 * @ngdoc filter
 * @name bizlifeservice.filter:getShortName
 * @function
 * @description
 * # getShortName
 * Filter in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .filter('getShortName', function () {
    function getShortName(uId) {
      var res = uId.split('-');
      return res[0];
    }

    return getShortName;
  });

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.businessTypeService
 * @description
 * # businessTypeService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('businessTypeService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var provider = $hostUrl;
    var service = {};
    service.delete = function (clase) {
      return $http.delete(provider.url() + "/api/businesstypes/" + clase.id);
    }
    service.search = function (pag, quantity) {
      return $http.get(provider.url() + "/api/businesstypes/" + pag + "/pag/" + quantity);
    }
    service.create = function(clase){
      var result = $http.post(provider.url() + '/api/businesstypes',  clase);
      return result;
    }
    service.update = function(cls){
      return $http.put(provider.url() + '/api/businesstypes/' + cls.id, cls);
    }
    return service;
  }]);
'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.applicationService
 * @description
 * # applicationService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('applicationService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var provider = $hostUrl;  
    var service = {};
    service.create = function (item) {
      return $http.post(provider.url() + '/api/applications', item);
    }
    service.getAll = function () {
      return $http.get(provider.url() + '/api/applications');
    }
    service.search = function (pag, quantity) {
      return $http.get(provider.url() + '/api/applications/' + pag + "/pag/" + quantity);
    }
    service.delete = function (item) {
      return $http.delete(provider.url() + "/api/applications/" + item.id);
    }
    service.update = function (item) {
      return $http.put(provider.url() + "/api/applications/" + item.id, item);
    }
    // service.getCurrent = function (shortName) {
    //   return $http.get(provider.url() + "/api/applications/" + shortName)
    // }
    service.postFileCloud = function (payload) {
      return $http({
        url: provider.url() + "/api/files",
        method: 'POST',
        data: payload,
        headers: { 'Content-Type': undefined },
        transformRequest: angular.identity
      });
    }
    return service;
  }]);  

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.templateService
 * @description
 * # templateService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('templateService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var provider = $hostUrl;
    var service={};
    // Public API here
    service.search = function(pag, quantity){
      var filter = {
        searchText: "",
        page: pag,
        quantity: quantity,
        nombre: ""
      };
      return $http.post(provider.url() + '/api/templates/filter',filter);
    }
    service.create = function(item){
      var result = $http.post(provider.url() + '/api/templates', item);
      return result;
    }
    service.delete = function(item){
      var result = $http.delete(provider.url() + '/api/templates/' + item.id);
      return result;
    }
    service.update = function(item){
      return $http.put(provider.url() + '/api/templates/' + item.id, item);
    }
    return service;
  }]);


'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.objectService
 * @description
 * # objectService
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('objectService', ["$http", "$hostUrl", function ($http, $hostUrl) {
    var service = {};
    var provider = $hostUrl;

    service.search = function(pag, quantity){
      var filter = {
        searchText: "",
        page: pag,
        quantity: quantity,
        url: "",
        uid: ""
      };
      return $http.post(provider.url() + '/api/objects/filter',filter);
    }
    return service;
  }]);

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:bzComment
 * @description
 * # bzComment
 */
angular.module('bizlifeservice')
  .directive('bzComment', ["commentService", function (commentService) {
    return {
      templateUrl: 'views/templates/bzcomment.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        if(attrs.uidApp){
          commentService.search(attrs.uidApp, attrs.quantity).
          then(function(response){
            scope.comments = response.data;
          }),function(response){
            console.log(response.data);
          };
        }
      }
    };
  }]);

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:dropDown
 * @description
 * # dropDown
 */
angular.module('bizlifeservice')
  .directive('bzApplications', function () {
    return {
      restrict: 'E',
      templateUrl:'views/templates/bzapplications.html',
      controller: 'DropdownAppsCtrl',
      scope: {},
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });


'use strict';

/**
 * @ngdoc function
 * @name bizlifeservice.controller:DropdownappsCtrl
 * @description
 * # DropdownAppsCtrl
 * Controller of the bizlifeservice
 */
angular.module('bizlifeservice')
  .controller('DropdownAppsCtrl', ["$scope", "applicationService", "$window", function ($scope, applicationService, $window) {
    $scope.items = [];
    $scope.cargarMas = true;
    $scope.pag = 1;

    $scope.getApps = function () {
      if($scope.items.length === 0){
        applicationService.search($scope.pag, 100).then(
          function (resp) {
            $scope.items = resp.data;
          }, angular.noop
        );
      }
    };

    $scope.goToAppInfo = function (urlApp) {
      $window.open(urlApp);
    };


  }]);

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:bzTitle
 * @description
 * # bzTitle
 */
angular.module('bizlifeservice')
  .directive('bzTitle', function () {
    return {
      templateUrl: 'views/templates/bztitle.html',
      restrict: 'E',
      scope:{
        title: '=title'
      },
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:bzUserprofile
 * @description
 * # bzUserprofile
 */
angular.module('bizlifeservice')
  .directive('bzUserprofile', function () {
    return {
      templateUrl: 'views/templates/bzprofile.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:bzAlert
 * @description
 * # bzAlert
 */
angular.module('bizlifeservice')
  .directive('bzAlert', function () {
    return {
      templateUrl: 'views/templates/bzalert.html',
      restrict: 'E',
      controller:'AlertCtrl',
      link: function postLink(scope, element, attrs) {
        
      }
    };
  });

'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:bzSearch
 * @description
 * # bzSearch
 */
angular.module('bizlifeservice')
.directive('bzSearch', function () {
  return {
    templateUrl: 'views/templates/bzsearchbar.html',
    restrict: 'E',
    scope: {
      searchText: '=',
      onSearchClick: '&',
      settings: '<',
      hideOptions: '=?'
    },
    link: function postLink(scope, element, attrs) {
      //search fun
      scope.search = function(){
        var options = { };
        scope.options.forEach(function(element){
          options[element.key] = element.value;
        });
        scope.onSearchClick({$options: options});
      };

      scope.writePlaceholder = function(){
        scope.placeholder = getPlaceholder();
      }

      function getPlaceholder(){
        var isAll = true;
        var placeholder = "";
        for (var i = 0; i<scope.options.length; i++) {
          var opt = scope.options[i];
          if(opt.value){
            if(!!placeholder){
              placeholder += ", ";
            }
            placeholder += opt.name
          } else {
            isAll = false;
          }
        }
        if(isAll){
          placeholder = "All";
        }
        if(!placeholder){
          placeholder = "None";
        }
        return placeholder;
      }
      
      //defaults
      scope.options = [];
      if(attrs.hideOptions === ''){
        scope.hideOptions = true;
      }
      //init
      if(!!scope.settings){
        scope.hideTitle = !scope.settings.title;
        scope.title = !scope.hideTitle ? scope.settings.title : '';
        
        if(!!scope.settings.options){
          scope.settings.options.forEach(function(element) {
            scope.options.push({
              key: element.key, 
              name: element.name, 
              value: true
            });
          });
        } else {
          scope.hideOptions = true;
        }
      } 
      //write 'All' as placeholder on init
      scope.writePlaceholder();
    }
  };
  });

'use strict';

/**
 * @ngdoc function
 * @name bizlifeservice.controller:AlertCtrl
 * @description
 * # AlertCtrl
 * Controller of the bizlifeservice
 */
angular.module('bizlifeservice')
.controller('AlertCtrl', ["$scope", "$rootScope", "$log", "alert", function ($scope, $rootScope, $log, alert) {
    
  $scope.alertLastUpdated = null;
  $scope.alertCount = null;
  $scope.alertMsgs = [];
  $scope.alertLoading = false;
  
  $rootScope.$on('event:alert-countChanged', function(event, count){
    if($scope.alertCount !== count){
      $scope.alertCount = count;
      getAlerts();
      $scope.$apply();
    } 
  });

  $rootScope.$on('event:connection-started', function(event){
    getCount();
    getAlerts();
  });

  $rootScope.$on('event:connection-error', function(event, error){
    $log.error(error);
  });
  
  function getCount() {
    alert.getCount(null).then(function(count){
      $scope.alertCount = count;
    }, function(e){
      $log.error(e);
    });
  }

  function getAlerts(){
    $scope.alertLoading = true;
    alert.getAll(null).then(function(result){
      $scope.alertLastUpdated = result.end;
      $scope.alertMsgs =  result.alerts;
      $scope.alertLoading = false;
      $scope.alertCount = result.alerts.length;
      if(!$scope.$$phase) {
        $scope.$apply();
      }
    }, function(e){
      $scope.alertLoading = false;
      $log.error(e);
    });
  }
}]);
'use strict';

/**
 * @ngdoc directive
 * @name bizlifeservice.directive:uiInclude
 * @description
 * # uiInclude
 */
angular.module('bizlifeservice')
  .directive('uiInclude', ["$http", "$templateCache", "$compile", function ($http, $templateCache, $compile) {
      var directive = {
        restrict: 'A',
        link: link
    };
    return directive;
    function link(scope, el, attr) {
        var templateUrl = scope.$eval(attr.uiInclude);
        $http.get(templateUrl, {cache: $templateCache}).success(
            function (tplContent) {
                el.replaceWith($compile(tplContent)(scope));
            }
        );
    }
  }]);

'use strict'; 
  /**
   * @ngdoc service
   * @name bizlifeservice.alert
   * @description
   * # alert
   * Factory in the bizlifeservice.
   */
  angular.module('bizlifeservice')
    .factory('alert', ["$hostUrl", "$rootScope", "$log", "HubConnectionFactory", function ($hostUrl, $rootScope, $log, HubConnectionFactory) {
      var hostUrl = $hostUrl.url();  
      var connection = HubConnectionFactory.create(hostUrl, "/alert");
      connection.on('countChanged', function(count) {
        $rootScope.$emit('event:alert-countChanged', count);
      });
      connection.onclose(function(e) {
        $log.info('socket is disconnected');
        $log.error(e);
        $rootScope.$emit('event:alert-disconnected');
      });
      connection.start().
        then(function() {
          $log.info('socket is connected');
          $rootScope.$emit('event:connection-started');
        }).
        catch(function(error) {
          $rootScope.$emit('event:connection-error', error);
          $log.error(error);
        });
      
      return {
        sendMessage: function (msg) {
          return connection.invoke('NewMessage', msg);
        },
        getAll: function (date) {
          return connection.invoke('GetAll', date);
        },
        getCount: function(date) {
          return connection.invoke('GetCount', date);
        }
      };
  }]);
  
'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.HubConnectionFactory
 * @description
 * # HubConnectionFactory
 * Factory in the bizlifeservice.
 */
angular.module('bizlifeservice')
  .factory('HubConnectionFactory', ["adalAuthenticationService", "$log", function (adalAuthenticationService, $log) {
    // Public API here
    return {
      create: function (hostUrl, hubRoute) {
        var url = hostUrl + hubRoute;
        var transportType = signalR.HttpTransportType.WebSockets;
        var options = {
          transport: transportType,
          accessTokenFactory: function () { 
            var resource = adalAuthenticationService.getResourceForEndpoint(url);
            if (resource === null) {
              $log.error("Can't find resource for endpoint " + url);
              return null;
            }
            var tokenStored = adalAuthenticationService.getCachedToken(resource);
            return tokenStored;
          }
        };
        return  new signalR.HubConnectionBuilder()
            .withUrl(url, options)
            .configureLogging(signalR.LogLevel.Information)
            .build();
      }
    };
  }]);

'use strict';

/**
 * @ngdoc service
 * @name bizlifeservice.userService
 * @description
 * # userService
 * Service in the bizlifeservice.
 */
angular.module('bizlifeservice')
.service('userService', ["$http", "$hostUrl", function ($http, $hostUrl) {
  var provider = $hostUrl;
  var service = {};

  service.getUsers = function () {
    return $http.get(provider.url() + '/api/users');
  };

  return service;
}]);

angular.module('bizlifeservice').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/templates/bzalert.html',
    "<div id=\"dropdown\"> <a href class=\"nav-link\" data-toggle=\"dropdown\"> <ng-md-icon icon=\"notifications\"></ng-md-icon> <span class=\"label up p-a-0 warn\">{{alertCount}}</span> </a> <div class=\"dropdown-menu text-color\" auto-close=\"outsideClick\"> <div class=\"scrollable\" style=\"max-height: 220px\"> <ul class=\"list-group list-group-gap m-a-0\"> <li class=\"list-group-item black lt box-shadow-z0 b\" ng-repeat=\"alert in alertMsgs\"> <span class=\"pull-left m-r\"> <img src=\"https://www.gravatar.com/avatar/{{alert.usuario|gravatar}}?d=mm\" alt=\"...\" class=\"w-40 img-circle\"> </span> <span class=\"clear block\">{{alert.msg}}</span><br> <small class=\"text-muted\">{{alert.moment}}</small>  </li> </ul> </div> </div> </div>"
  );


  $templateCache.put('views/templates/bzapplications.html',
    "<div id=\"dropdown\"> <a href class=\"nav-link\" ng-click=\"getApps()\" data-toggle=\"dropdown\"> <ng-md-icon icon=\"apps\"></ng-md-icon> <span class=\"label up p-a-0 accent\"></span> </a> <div class=\"dropdown-menu text-color\" auto-close=\"outsideClick\"> <div class=\"row\"> <div class=\"col\" ng-repeat=\"application in items\" ng-click=\"goToAppInfo(application.urlAccess)\"> <a class=\"p-a block text-center\"> <span class=\"w-40 avatar\"> <img ng-if=\"application.avatar == ''\" src=\"images/Default.jpg\"> <img ng-if=\"application.avatar != ''\" ng-src=\"{{application.avatar}}\"> </span> <span class=\"block\">{{application.name}}</span> </a> </div> </div> <!-- <a class=\"btn btn-sm b-info rounded\" ng-click=\"getMoreApps('6')\">Show More</a> --> </div> </div>"
  );


  $templateCache.put('views/templates/bzcomment.html',
    "<ul class=\"list-group\" ng-repeat=\"comment in comments\"> <li class=\"list-group-item\">{{comment.description}}</li> </ul>"
  );


  $templateCache.put('views/templates/bzfile.html',
    "<div class=\"row\"> <div class=\"col-sm-12 col-md-12\"> <div class=\"box p-a\"> <div class=\"pull-left m-r\"> <ng-md-icon icon=\"attach_file\" size=\"20\"> </ng-md-icon></div> <div class=\"m-y-sm pull-right\"> <a href=\"{{url}}\" download> <ng-md-icon icon=\"file_download\" size=\"24\"> </ng-md-icon></a> </div> <div> <a href=\"{{url}}\" download>{{url}}</a> <span ng-if=\"url.size\" class=\"text-muted text-sm\"> <strong>{{url}}</strong></span> </div> </div> </div> </div>"
  );


  $templateCache.put('views/templates/bzprofile.html',
    "<li> <a class=\"btn btn-link\" ng-hide=\"userInfo.isAuthenticated\" ng-click=\"login()\">Login</a> </li> <li class=\"nav-item dropdown\" ng-show=\"userInfo.isAuthenticated\"> <a href class=\"nav-link dropdown-toggle text-ellipsis\" data-toggle=\"dropdown\"> <span class=\"hidden-md-down nav-text m-l-sm text-right\"> <span class=\"_500\">{{userInfo.profile.given_name}} {{userInfo.profile.family_name}}</span> <small class=\"text-muted\">{{userInfo.userName}}</small> </span> <span class=\"avatar w-32\"> <img src=\"https://www.gravatar.com/avatar/{{userInfo.userName|gravatar}}?d=mm\" alt=\"...\"> </span> </a> <div class=\"dropdown-menu pull-right dropdown-menu-scale\"> <a class=\"dropdown-item\" ui-sref=\"app.userprofile\"> <span>Profile</span> </a> <div class=\"dropdown-divider\"></div> <a class=\"dropdown-item\" ng-click=\"logout()\">Log out</a> </div> </li>"
  );


  $templateCache.put('views/templates/bzsearchbar.html',
    "<span class=\"color-blue\" ng-hide=\"hideTitle\">{{title}}</span> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" name=\"x\" ng-attr-placeholder=\"{{placeholder}}\" ng-model=\"searchText\"> <div class=\"btn-group\" uib-dropdown dropdown-append-to-body auto-close=\"outsideClick\" ng-hide=\"hideOptions\"> <button id=\"btn-append-to-body\" type=\"button\" class=\"btn btn-primary\" uib-dropdown-toggle> <span class=\"caret\"></span> </button> <ul class=\"dropdown-menu\" uib-dropdown-menu role=\"menu\" aria-labelledby=\"btn-append-to-body\"> <li class=\"checkbox\" ng-repeat=\"opt in options\"> <label class=\"ui-check\"> <input type=\"checkbox\" ng-model=\"opt.value\" ng-change=\"writePlaceholder()\"> <i></i>{{opt.name}} </label> </li> </ul> </div> <span class=\"input-group-btn\"> <button class=\"btn btn-info\" type=\"button\" ng-click=\"search()\"> <ng-md-icon icon=\"search\" size=\"20\"></ng-md-icon> </button> </span> </div>"
  );


  $templateCache.put('views/templates/bztitle.html',
    "<!-- Open side - Naviation on mobile --> <a data-toggle=\"modal\" data-target=\"#aside\" class=\"hidden-lg-up mr-3\"> <ng-md-icon icon=\"menu\" size=\"20\"></ng-md-icon> </a> <!-- / --> <h4 class=\"mb-0 _300 m-l m-t\">{{title}}</h4>"
  );

}]);
